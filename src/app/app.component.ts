import { Component } from '@angular/core';
import {PopulationComponent} from './population/population.component';
import {HouseholdsComponent} from './households/households.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Singapore Survey Statistics';

  population = {
    path: 'population',
    component: PopulationComponent
  };

  households = {
    path: 'households',
    component: HouseholdsComponent
  };
}

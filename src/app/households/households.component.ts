import {Component, Injectable, OnInit} from '@angular/core';
import * as moment from 'moment/moment';
import {HttpClient, HttpParams} from '@angular/common/http';

@Component({
  selector: 'app-households',
  templateUrl: './households.component.html',
  styleUrls: ['./households.component.css']
})
@Injectable()
export class HouseholdsComponent implements OnInit {

  BaseUrl = 'http://localhost:5000';
  Api = '/api/v1/households';
  State = '';
  Result = [];

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  onClick() {
    if (this.State === '') {
      alert('Please input at least 1 state to find household income');
      return;
    }
    this.Result = [];
    const params = new HttpParams();
    params.set('State', this.State);
    const clickTick = moment().format('YYYY-MM-DD HH:mm:ss.SSS');
    const httpUrl = this.BaseUrl + this.Api + '?State=' + this.State;
    console.log(clickTick + ' : [population] : ' + httpUrl);

    this.http.get<HouseholdsResult[]>(httpUrl).subscribe(
      data => {
        const dataReceiveTime = moment().format('YYYY-MM-DD HH:mm:ss.SSS');
        data.forEach( e => {
          console.log(dataReceiveTime + ' : ' + JSON.stringify(e));
          this.Result.push(new HouseholdsResult(e.state, e.households));
        });
      }
    );
  }
}

/**
 * Server result: This class is required to map server response type.
 */
class HouseholdsResult {
  state: number;
  households: number;

  constructor(state, households) {
    this.state = state;
    this.households = households;
  }

}

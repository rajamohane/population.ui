import {Component, Injectable, OnInit} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';
import * as moment from 'moment/moment';

@Component({
  selector: 'app-population',
  templateUrl: './population.component.html',
  styleUrls: ['./population.component.css']
})
@Injectable()
export class PopulationComponent implements OnInit {
  BaseUrl = 'http://localhost:5000';
  Api = '/api/v1/population';
  State = '';
  Result = [];

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  onClick() {
    if (this.State === '') {
      alert('Please input at least 1 state to find population');
      return;
    }
    this.Result = [];
    const params = new HttpParams();
    params.set('State', this.State);
    const clickTick = moment().format('YYYY-MM-DD HH:mm:ss.SSS');
    const httpUrl = this.BaseUrl + this.Api + '?State=' + this.State;
    console.log(clickTick + ' : [population] : ' + httpUrl);

    this.http.get<PopulationResult[]>(httpUrl).subscribe(
      data => {
        const dataReceiveTime = moment().format('YYYY-MM-DD HH:mm:ss.SSS');
        data.forEach( e => {
          console.log(dataReceiveTime + ' : ' + JSON.stringify(e));
          this.Result.push(new PopulationResult(e.state, e.population));
        });
      }, err => {
        alert('error occurred, please check console log for more details');
        console.log(err.toString());
      }
    );
  }
}

class PopulationResult {
  state: number;
  population: number;

  constructor(state, population) {
    this.state = state;
    this.population = population;
  }

}
